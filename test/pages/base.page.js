export default class Page {

    open(path) {
        browser.url(path)
    }

    SelectItemFromDropDown() {
        browser.pause(2000)
        browser.keys("\uE015")
        browser.keys("\uE015")
        return browser.keys("\uE007")
    }


    ClickOn(element) {
        if(!element.isDisplayed()){
           browser.pause(5000)
        }
        return element.click()
    }

    EnterData(element, data) {
        return element.setValue(data)
    }


    SelectItem(element, data) {

        browser.pause(5000)
        return element.selectByAttribute("value", data)

    }


    WaitForElementToBeLoaded(element) {
        if(element.isDisplayed()){

            element.waitForDisplayed(undefined)
        }
        browser.pause(3000)
    }

    IsElementPresent(element) {
        browser.pause(4000)
        return element.isDisplayed()
    }

    IsElementExist(element) {
        browser.pause(3000)
        return element.isExisting()
    }

    GetTextFromElement(element) {
        return element.getText()
    }



}