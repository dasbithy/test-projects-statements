import Page from '../pages/base.page';
class Statement extends Page {

  get _search() {
    return $("//button[@class='btn btn-primary']")
  }
  get _account() {
    return $("#search_account_id")
  }
  get _balancetable() {
    return $("#search_account_id")
  }
  get _beforebalance() {
    return $("/html[1]/body[1]/div[1]/div[1]")
  }
  get _afterbalance() {
    return $("//div[@class='col-sm']")
  }
  //  ----------------------------------------------------------
  GoToStatementPage() {
    browser.url('/')
  }

  EnterAccountId(accountID) {
    return this._account.setValue(accountID)
  }

  EnterSearch() {
    return super.ClickOn(this._search)
  }
  CheckBeforeBalance() {

    const listOfTagsOfMoney = $$('<td />');
    const moneyAmountArr = [];
    var realBeforeBalance = 0;
    var i;
    listOfTagsOfMoney.forEach(element => {
      if (element.getAttribute('id') == "money") {
        moneyAmountArr.push(element.getText());
      }
    });

    for (i = 0; i < moneyAmountArr.length - 1; i++) {
      realBeforeBalance += parseFloat(moneyAmountArr[i].split(" ")[0]);
    }
    var realAfterBalance = realBeforeBalance + parseFloat(moneyAmountArr[moneyAmountArr.length - 1].split(" ")[0]);

  }
  VerifyBeforeBalance() {
    var beforebalance = this._beforebalance.getText();
    var result = parseFloat(beforebalance.slice(16, 22));


    return result;
  }
  VerifyAfterBalance() {

    var afterbalance = this._afterbalance.getText();
    var textLength = afterbalance.length;
    var sliceStartValue = parseFloat(textLength - 11)
    var sliceEndValue = parseFloat(textLength - 4)
    var result = parseFloat(afterbalance.slice(sliceStartValue, sliceEndValue));

    return result;
  }
  RealBeforeBalance() {

    const listOfTagsOfMoney = $$('<td />');
    const moneyAmountArr = [];
    var realBeforeBalance = 0;
    var i;
    listOfTagsOfMoney.forEach(element => {
      if (element.getAttribute('id') == "money") {
        moneyAmountArr.push(element.getText());
      }
    });

    for (i = 0; i < moneyAmountArr.length - 1; i++) {
      realBeforeBalance += parseFloat(moneyAmountArr[i].split(" ")[0]);
    }

    return realBeforeBalance;
  }
  RealAfterBalance() {

    const listOfTagsOfMoney = $$('<td />');
    const moneyAmountArr = [];
    var realBeforeBalance = 0;
    var i;
    listOfTagsOfMoney.forEach(element => {
      if (element.getAttribute('id') == "money") {
        moneyAmountArr.push(element.getText());
      }
    });

    for (i = 0; i < moneyAmountArr.length - 1; i++) {
      realBeforeBalance += parseFloat(moneyAmountArr[i].split(" ")[0]);
    }
    var realAfterBalance = realBeforeBalance + parseFloat(moneyAmountArr[moneyAmountArr.length - 1].split(" ")[0]);
    return realAfterBalance;

  }

}
export default new Statement()