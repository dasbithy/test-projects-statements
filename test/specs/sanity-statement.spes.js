import statement from '../pages/statements.page'; 

describe("Run test for statement application  ",()=>{

    it("verify the Before balance for an account ", () => {
        statement.GoToStatementPage();
        browser.pause(6000);
        //Enter the account id from the EnterAccountId
        statement.EnterAccountId("3")
        statement.EnterSearch();
        browser.pause(6000);
        assert.equal(statement.VerifyBeforeBalance(),statement.RealBeforeBalance()) 
        
        
    })
    it("verify the After balance for an account ", () => {
        statement.GoToStatementPage();
        browser.pause(6000);
        //Enter the account id from the EnterAccountId
        statement.EnterAccountId("3")
        statement.EnterSearch();
        assert.equal(statement.VerifyAfterBalance(),statement.RealAfterBalance())
        
    })

    it("verify the before balance for all transaction ", () => {
        statement.GoToStatementPage();
        browser.pause(6000);
        statement.EnterSearch();
        assert.equal(statement.VerifyBeforeBalance(),statement.RealBeforeBalance()) 
        
    })
    it("verify the After balance for all transaction ", () => {
        statement.GoToStatementPage();
        browser.pause(6000);
        statement.EnterSearch();
        assert.equal(statement.VerifyAfterBalance(),statement.RealAfterBalance())
        
    })


});